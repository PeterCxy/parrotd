#[derive(Debug)]
pub enum ParrotError {
    IconPackNotFoundError,
    InvalidIconPackError,
    IOError(std::io::Error),
}

pub type Result<T> = std::result::Result<T, ParrotError>;

macro_rules! try_io {
    ($x:expr) => {
        $x.map_err(ParrotError::IOError)?
    }
}
