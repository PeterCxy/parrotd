use crate::*;
use std::path::{Path, PathBuf};

#[allow(dead_code)]
#[derive(Debug)]
pub enum PackVariant {
    Light,
    Dark,
}

impl PackVariant {
    fn as_str(&self) -> &'static str {
        match self {
            PackVariant::Light => "light",
            PackVariant::Dark => "dark",
        }
    }

    pub fn parse(s: &str) -> Option<PackVariant> {
        match s {
            "light" => Some(PackVariant::Light),
            "dark" => Some(PackVariant::Dark),
            _ => None
        }
    }
}

#[allow(unused)]
pub struct IconPack {
    path: PathBuf,
    name: String,
    variant: PackVariant,
    icons: Vec<String>,
    num_icons: usize,
    cur_icon: usize,
}

impl IconPack {
    pub fn new<P: AsRef<Path>>(root_path: P, name: &str, variant: PackVariant) -> Result<IconPack> {
        let mut path = try_io!(root_path.as_ref().canonicalize());
        path.push(name);

        let prefix = format!("{}_{}_", variant.as_str(), name);
        let mut icons = vec![];

        if !try_io!(path.try_exists()) {
            return Err(ParrotError::IconPackNotFoundError);
        }

        for entry in try_io!(std::fs::read_dir(path.clone())) {
            let entry = try_io!(entry);
            if try_io!(entry.metadata()).is_dir() {
                continue;
            }

            let name = match entry.file_name().to_str() {
                Some(name) => name.to_owned(),
                None => continue
            };

            if !name.starts_with(&prefix) {
                continue;
            }

            if !name.ends_with(".png") {
                continue;
            }

            icons.push(name.split_at(name.len() - ".png".len()).0.to_string());
        }

        alphanumeric_sort::sort_str_slice(&mut icons); // Alphanumerical ordering

        let num_icons = icons.len();
        
        if num_icons == 0 {
            Err(ParrotError::IconPackNotFoundError)
        } else {
            Ok(IconPack {
                path,
                name: name.to_string(),
                variant,
                icons,
                num_icons,
                cur_icon: 0,
            })
        }
    }

    pub fn pack_path(&self) -> &str {
        // Used for libappindicator icon theme path
        // We have already checked for UTF-8 violations in new
        return self.path.to_str().unwrap()
    }

    pub fn next_icon_path(&mut self) -> &str {
        let ret = &self.icons[self.cur_icon];
        self.cur_icon += 1;
        if self.cur_icon >= self.num_icons {
            self.cur_icon = 0;
        }
        return ret;
    }
}
